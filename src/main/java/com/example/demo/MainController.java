package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.tempuri.CalculatorSoap;

@RestController
public class MainController {

	@Autowired
	private CalculatorSoap calculator;

	@GetMapping("/")
	public String hello() {
		return "Hello world";
	}

	@GetMapping("/test/{a}/{b}")
	public int test(@PathVariable("a") int intA, @PathVariable("b") int intB) {
		return calculator.add(intA, intB);
	}
}
