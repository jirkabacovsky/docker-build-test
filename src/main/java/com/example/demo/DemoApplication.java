package com.example.demo;

import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import org.tempuri.Calculator;
import org.tempuri.CalculatorSoap;

@Configuration
@RestController
@SpringBootApplication
public class DemoApplication {

	@Value("${weather.url:http://www.dneonline.com/calculator.asmx?WSDL}")
	private String weatherUrl;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	CalculatorSoap calculator() {
		try {
			Calculator service = new Calculator(new URL(weatherUrl));
			return service.getCalculatorSoap();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
