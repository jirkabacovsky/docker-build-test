FROM adoptopenjdk:11-jre-hotspot
ADD target/*.jar /app/app.jar
ENTRYPOINT ["java", "-Xmx256m", "-Duser.timezone=UTC", "-jar", "/app/app.jar"]
EXPOSE 8080